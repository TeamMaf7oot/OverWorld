package place;


public class Tip {
	private int likes;
	private String tips;
	
	public Tip(int likes, String tips) {
		super();
		this.likes = likes;
		this.tips = tips;
	}
	
	public int getLikes() {
		return likes;
	}
	public void setLikes(int likes) {
		this.likes = likes;
	}
	public String getTips() {
		return tips;
	}
	public void setTips(String tips) {
		this.tips = tips;
	}
	
	

}
