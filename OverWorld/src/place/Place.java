package place;

import overworld.DBH2;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Place {

    private String placeName;
    private int user_id;
    private Location location;
    private ArrayList<String> tastes;
    private ArrayList<Tip> tips;
    private int rate;
    private int numOfCheckIns;
    private int numberOfSaveProcess;
    private int place_id;

    public Place() {
        tastes = new ArrayList<String>();
        tips = new ArrayList<Tip>();

    }

    public Place(String placeName, int user_id, Location location, ArrayList<String> tastes, ArrayList<Tip> tips, int rate,
            int numberOfSaveProcess, int numOfCheckIns, int place_id) {
        this.placeName = placeName;
        this.user_id = user_id;
        this.location = location;
        this.tastes = tastes;
        this.tips = tips;
        this.rate = rate;
        this.numberOfSaveProcess = numberOfSaveProcess;
        this.numOfCheckIns = numOfCheckIns;
        this.place_id = place_id;

    }

    public Place(String placeName, int user_id, Location location, ArrayList<String> tastes,
            Tip tip) {
        this.placeName = placeName;
        this.user_id = user_id;
        this.location = location;
        this.tastes = tastes;
        tips = new ArrayList<Tip>();
        tips.add(tip);
        rate = 0;
        numberOfSaveProcess = 0;
        numOfCheckIns = 0;
    }


    public void setplace_id(int id) {
        place_id = id;
    }

    public int getplace_id() {
        return place_id;
    }

    public void setTaste(String text) {
        tastes.add(text);
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public void set_userId(int user_id) {
        this.user_id = user_id;
    }

    public int get_userId() {
        return user_id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public ArrayList<String> getTastes() {
        return tastes;
    }

    public void setTastes(ArrayList<String> tastes) {
        this.tastes = tastes;
    }

    public ArrayList<Tip> getTips() {
        return tips;
    }

    public void setTips(ArrayList<Tip> tips) {
        this.tips = tips;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
    
    public int getNumberOfSaveProcess() {
        return numberOfSaveProcess;
    }

    public void setNumberOfSaveProcess(int numberOfSaveProcess) {
        this.numberOfSaveProcess = numberOfSaveProcess;
    }

    public void increase_numOfSaveCheckIn() {
        numOfCheckIns++;
    }

    public void set_numOfCheckIns(int number) {
        numOfCheckIns = number;
    }

    public int get_numOfCheckIns() {
        return numOfCheckIns;
    }

    public void set_in_database() throws SQLException {
       DBH2 db = new DBH2();
        try {
            db.open_connection();
            
            ResultSet rs = db.executequery("SELECT tip_id FROM Tips");

            ////////////get last id of tips in database 
            String id = null;
            int max = -1;
            while (rs.next()) {
                id = rs.getString("tip_id");
                if (Integer.valueOf(id) > max) {
                    max = Integer.valueOf(id);
                }
            }
            int tip_id = max;
            tip_id++;
            db.executequery("INSERT INTO Tips (tip_id ,likes ,text ) VALUES (" + tip_id + ",0,'" + tips.get(0).getTips() + "');");

            ////////////get last id of tastes in database
            rs = db.executequery("SELECT taste_id FROM Taste");
            max = -1;
            while (rs.next()) {
                id = rs.getString("taste_id");
                if (Integer.valueOf(id) > max) {
                    max = Integer.valueOf(id);
                }
            }
            int taste_id = max;
            taste_id++;
            for (int i = 0; i < tastes.size(); i++) {
                db.executequery("INSERT INTO Taste (taste_id ,name ) VALUES (" + taste_id + " , '" + tastes.get(i) + "');");
            }

            //////get last id in places from database
            rs = db.executequery("SELECT place_id FROM place");
            while (rs.next()) {
                id = rs.getString("place_id");
            }
            int place_id_ = Integer.valueOf(id);
            place_id_++;
            db.executequery("INSERT INTO place (place_id ,tip_id ,taste_id ,place_name ,numOfCheckIn ,rate ,numOfSaveProcess ,Location_ID  ,user_id) VALUES (" + place_id_ + " , " + tip_id + " ," + taste_id + ",'" + placeName + "', 0, 0, 0,'" + location.toString() + "' ," + user_id + ");");
        } catch (Exception e) {

        } finally {
            try {
                db.close_connection();
            } catch (Exception e) {

            }
        }
    }
}
