package place;

import userprofile.User;
import overworld.DBH;
import overworld.DBH2;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PlaceController {

    private Place place;
    
    public void addPlace(String name, String tip, int user_id, int lat, int long_, ArrayList<String> taste) {
        Tip Tips = new Tip(0, tip);
        Location loc = new Location(lat, long_);
        place = new Place(name,user_id, loc, taste, Tips);
        try {
            place.set_in_database();
        } catch (SQLException ex) {
            Logger.getLogger(PlaceController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void addTip(Tip tip) ///later not in this phase 
    {

    }

    public void addTaste(String text) throws SQLException {
        DBH2 db=new DBH2();
        ResultSet rs=null;
        try{
            db.open_connection();
        rs = db.executequery("SELECT taste_id FROM place WHERE place_id =" + place.getplace_id());
        String taste_id = rs.getString("taste_id");
        db.executequery("INSERT INTO Taste ( taste_id , name ) VALUES (" + taste_id + ", '" + text + "')");
        }
        catch(Exception e){
        }finally{
        try{
            if(rs!=null)
                rs.close();
            db.close_connection();
        }catch(Exception e){
        }
        }
    }

    public void deleteTaste(int index) {

    }
    
    public CheckIn add_checkin( String text)
    {
        CheckIn checkin =new CheckIn(text , place );
        
        return checkin;
    }
    
    public void Update_numOfCheckIns()  {
       
       DBH2 D=new DBH2();
       int number=place.get_numOfCheckIns();
       number++;
       place.increase_numOfSaveCheckIn();
       try{
           D.open_connection();
       D.executeupdate("UPDATE place SET numOfCheckIn ="+number +" WHERE place_id ="+ place.getplace_id());
       }catch(Exception e)
       {
       
       }finally
       {
       D.close_connection();
       }
        UpdateRate(1);
    }

    public void deleteTip(int index) {

    }

    public void UpdateTip(int index, String text) {

    }

    public void UpdateCheckIn(int index, String text) {

    }

    public void deleteCheckIn(int index) {

    }

    public void UpdateNumOfSaveProcess() {

    }

    public void UpdateNumLikeTip(int index, int number) {

    }

    public void addLikeToCheckIn(int index) {

    }

    public void deleteLikeToCheckIn(int index) {

    }

    public void addCommentOfToCheckIn(String text) {

    }

    public void deleteCommentOfToCheckIn(int index) {

    }

    public void addNotiOfCheckIntoFriends() {

    }

    public void getNearbyPlaces() {

    }

    public int get_rate() {
        return place.getRate();
    }

    public String get_name() {
        return place.getPlaceName();
    }

    public Tip get_tip() {
        return place.getTips().get(0);
    }


    public ArrayList<String> get_tastes() {
        return place.getTastes();
    }

    public void update_numOfsaveProcess()
    {
        int number=place.getNumberOfSaveProcess();
        number++;
        place.setNumberOfSaveProcess(number);
        DBH2 D=new DBH2();
        try{
            D.open_connection();
            D.executeupdate("UPDATE place SET numOfSaveProcess ="+number+"WHERE place_id ="+place.getplace_id());
        }catch(Exception e)
        {
        }finally 
        {
        D.close_connection();
        }
        UpdateRate(1);
        
    }
 
    public void home_places( ArrayList<String> name,ArrayList<Integer> place_id)
    {
    DBH2 D=new DBH2();
    ResultSet rs=null;
    try{
        D.open_connection();
        rs=D.executequery("SELECT place_name , place_id FROM place ");
        while(rs.next())
        {
        name.add(rs.getString("place_name"));
        place_id.add(Integer.valueOf(rs.getString("place_id")));
        
        }
        }catch(Exception e)
        {
        
        
        }finally 
    {
    try{
        if(rs!=null)
            rs.close();
        D.close_connection();
    
    
    
    }catch(Exception e)
    {
    
    
    }
    
    }
    
    
    }
    public Place getPlace(int place_id) {
        ResultSet rs=null;
        DBH2 D=new DBH2();
        try {
            D.open_connection();
            rs=D.executequery("SELECT * FROM place WHERE place_id = "+place_id+";");
            String name = rs.getString("place_name");
            String numofCheckin = rs.getString("numOfCheckIn");
            String user_id= rs.getString("user_id");
            String rate = rs.getString("rate");
            String numOfSaveProcess = rs.getString("numOfSaveProcess");
            String location_id = rs.getString("location_id");
            String taste_id = rs.getString("taste_id");
            String tip_id = rs.getString("tip_id");
            ArrayList<String> tastes = new ArrayList<String>();
            
            rs.close();
            rs=null;
            ///////////////////get tastes 
            rs = D.executequery("SELECT name FROM Taste WHERE taste_id = " + taste_id + ";");
            while (rs.next()) {
                tastes.add(rs.getString("name"));
            }
            rs.close();
            rs = null;

            /////////////////get tips 
            ArrayList<Tip> tips = new ArrayList<Tip>();
            rs = D.executequery("SELECT * FROM Tips WHERE tip_id = " + tip_id + ";");
            while (rs.next()) {
                Tip t = new Tip(Integer.valueOf(rs.getString("likes")), rs.getString("text"));
                tips.add(t);
            }

            rs.close();
            rs = null;

            String loc[] = location_id.split(",");
            int lat = Integer.valueOf(loc[0]);
            int long_ = Integer.valueOf(loc[1]);
            Location location = new Location(lat, long_);
            place = new Place(name, Integer.valueOf(user_id) ,location , tastes ,tips ,Integer.valueOf(rate),Integer.valueOf(numOfSaveProcess) ,Integer.valueOf(numofCheckin) ,place_id );
      
        } catch (Exception e) {

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
               D.close_connection();
            } catch (SQLException ex) {
                Logger.getLogger(PlaceController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return place;
    }

    public void UpdateRate(int number) {
        DBH2 D=new DBH2();
        ResultSet rs=null;
        try {
            D.open_connection();
            rs=D.executequery("SELECT rate FROM place WHERE place_id ="+place.getplace_id());
            int rate =Integer.valueOf(rs.getString("rate"));
            rate*=10;
            rate+=number;
            rate/=10   ;        
            D.executeupdate("UPDATE place SET rate = " + rate + " WHERE place_id = " + place.getplace_id()+ ";");

        } catch (Exception e) {

        } finally {
           try
           {
               if(rs!=null)
                   rs.close();
               D.close_connection();}
           catch(Exception e)
           {
           
           }
        }
    }

    public int get_user_id()
    {
    return place.get_userId();
    }
    
    
}
