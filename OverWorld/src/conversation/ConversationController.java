package conversation;

import userprofile.User;
import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import home.Notification;

public class ConversationController {

    private String sender;
    static int indxConversation;
    private ArrayList<Conversation> inbox;
    private Conversation conv;// to notify
    private ConversationDBH databaseHandler = new ConversationDBH();

    public ConversationController() {
//		databaseHandler = new ConversationDBH();
    }

    public ConversationController(User person, String mess, String converName) {
      //  System.out.print("In conversation controller");
        conv = new Conversation(person, mess, converName);
//        mess = "hello\n";
//        //one = "Amira";
//        converName = "First";
        System.out.print("I am in conversation controller\n");
//        person.setID(0);
//        person.setName("Amira");
      //  System.out.println(mess+" "+converName + " "+person.getID()+" "+person.getName());
        databaseHandler.insert(mess, 0, indxConversation++, converName, person.getID(), person.getName());
          System.out.print("In conversation controller");
    }

    public ConversationController(ArrayList<User> people, ArrayList<String> message, String converName) {
        conv = new Conversation(people, message, converName);
        if (message.size() < 0) {
            for (int i = 0; i < people.size(); i++) {
                databaseHandler.insert("", 0, indxConversation, converName, people.get(i).getID(), people.get(i).getName() );
            }
        } else {
            for (int i = 0; i < message.size(); i++) {
                databaseHandler.insert(message.get(i), i, indxConversation, converName, people.get(i).getID(),people.get(i).getName());
            }
            indxConversation++;
        }
        databaseHandler.setNoti(indxConversation, message.get(0));
    }
///
     public ConversationController(ArrayList<User> people, String converName) {
        conv = new Conversation(people, "", converName);
            for (int i = 0; i < people.size(); i++) {
                databaseHandler.insert("", 0, indxConversation, converName, people.get(i).getID(), people.get(i).getName() );
            }
     
            indxConversation++;
            databaseHandler.setNoti(indxConversation,"Added to "+converName );
        }

    //
    public void addMessage(User sender,String message, int indxCov, String converName) {
        Conversation conv = databaseHandler.selectConversation(indxCov);
        // added message to conversation in datbase
        if (conv.getConversation().size() > 0) {
            databaseHandler.insert(message, conv.getConversation().size(), indxCov, converName,sender.getID(),sender.getName());
        } else {
            databaseHandler.update(indxCov, message, sender.getID());
        }
        databaseHandler.setNoti(indxCov, message);
    }

    public void addPeople(User one, int indxConv, String converName) {
        Conversation conv = databaseHandler.selectConversation(indxConv);
// get conversation indxConv from database and put it in conv;
        if ((conv.getPeople()).size() > 1) {
            databaseHandler.insert("", 0, indxConv, converName, one.getID(),one.getName());
        } else {
            ArrayList<User> pp = new ArrayList<User>();
            pp.add(one);
            ArrayList<String> message = new ArrayList<String>();
            Conversation conversation = new Conversation(pp, message, converName);
            databaseHandler.insert("", 0, indxConv++, converName,pp.get(0).getID(), pp.get(0).getName());
        }
    }

    public ArrayList<Conversation> getInbox() {
        inbox = databaseHandler.select();
        return inbox;
    }

    void deletePeople(int indx, int indxConv) {
        try {
            //delete conversation from the User ;
        } catch (Exception e) {
            e.printStackTrace();// null pointer
        }
    }
//	public Notification getNotification() {// which Notification ?????????????
//
//		Notification noti = new Notification();
//		// get noti from database
//		return noti;
//	}

    public void deleteMessage(int indxConv, int indxMess) {
        Conversation convers = new Conversation();
        convers = databaseHandler.selectConversation(indxConv);
        databaseHandler.deletMessage(indxConv, indxMess, sender);
    }

    public Conversation getConversation(int indxConv) {
        Conversation convers = new Conversation();
        convers = databaseHandler.selectConversation(indxConv);
        return convers;
    }

    public void deleteConversation(int indxConversation) {
        databaseHandler.delete(indxConversation);
    }
}
