package conversation;

import overworld.DBH;
import userprofile.User;
import conversation.Conversation;
import conversation.Pair;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import home.NotificationController;
import overworld.DBH2;

public class ConversationDBH {

    public int notid;
    Format formatter = new SimpleDateFormat("E MMM d HH:mm:ss zzz yyyy ");
    String today = formatter.format(new Date());
    // SELECT columnName FROM tableName where condition
    // DELETE FROM tableName [WHERE expr]
    // INSERT INTO tableName [(column-list)] VALUES(value-list)
    // UPDATE tableName SET assignment [, assignment]* [WHERE expr]

    public ConversationDBH() {
        // TODO Auto-generated constructor stub

    }
    public void update(int indxConv, String message, int sender) {
        DBH2 database = new DBH2();

        try {
            database.open_connection();
            String id = Integer.toString(indxConv);
            String UserId = Integer.toString(sender);
            String query = "UPDATE Conversation SET  Message = '" + message + "'," + "MessageID=" + "0" + " WHERE UserID =" + UserId + " and" + " ConversationID =" + id;
            database.executeupdate(query);
        } catch (Exception e) {

        } finally {
            database.close_connection();
        }
    }

    public Conversation selectConversation(int indx) {
        Conversation conv = new Conversation();
        String i = Integer.toString(indx);
        String query = "SELECT * FROM Conversation where ConversationID=" + i;
        DBH2 database = new DBH2();

        try {
            database.open_connection();
            ResultSet rs = database.executequery(query);
            try {
                while (rs.next()) {
                    Pair chat = new Pair();
//                    System.out.print(rs.getString("User")+" "+rs.getInt("UserID"));
                    String message = rs.getString("Message");
                    User person = new User();
                    person.setName(rs.getString("User"));
                    person.setID(rs.getInt("UserID"));
                    
                    conv.setConversation(person, message);
                    System.out.print(person.getID()+" "+message);

                    System.out.println("\n conversation : "+conv.getPeople());
                }
            } catch (SQLException ex) {
                Logger.getLogger(home.NotificationController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (Exception e) {

        } finally {
            database.close_connection();
        }
        return conv;
    }

    public ArrayList<Conversation> select() {
        ArrayList<Conversation> inbox = new ArrayList<Conversation>();
        DBH2 database = new DBH2();

        try {
            database.open_connection();
            String query = "SELECT * FROM Conversation";
            ResultSet rs = database.executequery(query);
            try {
                while (rs.next()) {
                    // System.out.println(rs.getInt("noti_id"));
                    User person = new User();
                    person.setName(rs.getString("User"));
                    person.setEmail(rs.getString("UserID"));
                    String message = rs.getString("Message");
                    String converName = rs.getString("converName");
                    int indxConv = rs.getInt("ConversationID");
                    int indxMessage = rs.getInt("MessageID");
                    Conversation conv = new Conversation(person, message, converName);
                    if (indxConv < inbox.size()) {
                        Conversation tmpConv = inbox.get(indxConv);
                        tmpConv.setConversation(person, message);
                        inbox.set(indxConv, conv);
                    } else {
                        inbox.add(conv);
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(home.NotificationController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (Exception e) {

        } finally {
            database.close_connection();
        }
        // add to inbox;
        return inbox;
    }
    //   CREATE TABLE "Conversation" ("User"  ,
    //"ConversationID" 
    //"MessageID" ,"Message" text,
    //"NotificationID" 
    //"Date" DATETIME)
    // SELECT columnName FROM tableName where condition
    // DELETE FROM tableName [WHERE expr]
    // INSERT INTO tableName [(column-list)] VALUES(value-list)
    // UPDATE tableName SET assignment [, assignment]* [WHERE expr]

    public void insert(String mess, int indxMess, int indxConv, String converName, int userID,String sender) {
        //insert(mess, 0, indxConversation++, converName, person.getID(), person.getName());
        DBH2 D = new DBH2();
        try {
            D.open_connection();
            System.out.print("in insert");
            String indx = Integer.toString(indxConv);
            String indxmessage = Integer.toString(indxMess);
            String usID = Integer.toString(userID);
            String notId = Integer.toString(notid);
            String query
                    = "INSERT INTO Conversation (User,UserID,ConversationID, converName,MessageID, Message,NotificationID) VALUES ('"
                    + sender + "'," + usID + "," + indx + ",'" + converName + "'," + indxmessage + ",'" + mess + "'," + notId + ")";
            System.out.print("I made a query");
            D.executeupdate(query);

        } catch (Exception e) {
            System.out.println("here");
        } finally {
            D.close_connection();
        }
    }

    public void delete(int indxConv) {
        DBH2 database = new DBH2();
        try {
            database.open_connection();
            String indx = Integer.toString(indxConv);
            String query = "DELETE Conversation WHERE ConversationID = " + indx;
            database.executeupdate(query);
        } catch (Exception e) {
            System.out.println("here");
        } finally {
            database.close_connection();
        }
    }

    public void deletMessage(int indxConv, int indxMess, String sender) {
        DBH2 database = new DBH2();
        try {
            database.open_connection();
            String indx = Integer.toString(indxConv);
            String indxMessage = Integer.toString(indxMess);
            String userId = sender;
            String query = "DELETE FROM Conversation WHERE ConversationID = " + indx + "and MessageID =" + indxMessage + "and UserID=" + userId;
            database.executeupdate(query);
        } catch (Exception e) {
            System.out.println("here");
        } finally {
            database.close_connection();
        }
    }

    public void setNoti(int convID, String message) {
        // how knowing who user has which conversation
        DBH2 database = new DBH2();
        try {
            database.open_connection();
            String query = "select noti_id from Notification ";
            ResultSet rs = database.executequery(query);
            notid = 0;
            try {
                while (rs.next()) {
                    notid = rs.getInt("noti_id");
                }
            } catch (SQLException ex) {
                Logger.getLogger(home.NotificationController.class.getName()).log(Level.SEVERE, null, ex);
            }
            notid++;

            String notiID = Integer.toString(notid);
            String convId = Integer.toString(convID);
            query = "select UserID from Conversation where ConversationID =" + convId;
            rs = database.executequery(query);
            try {
                while (rs.next()) {
                    if(rs.getInt("UserID") == signinregister.SiginController.me.getID())continue;
                    int id = rs.getInt("UserID");
                    String user_id = Integer.toString(id);
                    query = "INSERT INTO Notification "
                            + "(kind, noti_id, conv_id,user_id, text,is_seen) VALUES("
                            + "'message'," +notiID + "," + convId + "," + user_id + ",'" + message + "'," + "'false')";
                }
            } catch (SQLException ex) {
                Logger.getLogger(home.NotificationController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (Exception e) {
            System.out.println("here");
        } finally {
            database.close_connection();
        }
    }
}
