package conversation;
import userprofile.User;
import java.util.ArrayList;


/**
 * 
 */

/**
 * @author aashmawy
 *
 */
class Pair {
	public String message;
	public User person;   

    @Override
    public String toString() {
        return "Pair{" + "message=" + message + ", person=" + person + '}';
    }
        
}

public class Conversation {
	// add to database
	/**
	 * 
	 */
        private String name; 
	private ArrayList<Pair> conversation = new ArrayList<Pair>();

    @Override
    public String toString() {
        String rt="";
        rt+= "Conversation{" + "conversation=" ;
        for (Pair pair : conversation) {
            rt+=pair.toString();
        }
        rt+="}";
        return rt;
    }

	public Conversation(ArrayList<User> person, ArrayList<String>message, String name) {
           // conversation = new ArrayList<Pair>(); 
            this.name = name;
            for(int i=0 ;i < person.size(); i++){
                Pair chat = new Pair();
                chat.person = person.get(i);
                if(message.size() > i )
                    chat.message = message.get(i);
                else chat.message = "";
                conversation.add(chat);
            }
	}
        public Conversation(ArrayList<User> person, String message, String name) {
           // conversation = new ArrayList<Pair>(); 
            this.name = name;
            for(int i=0 ;i < person.size(); i++){
                Pair chat = new Pair();
                chat.person = person.get(i);
                    chat.message = message;
             
                conversation.add(chat);
            }
	}
        public void setName(String name){
            this.name =name;
        }
        public String getName(){
                return name;
        }
	public Conversation(User one, String message, String name) {
                this.name = name; 
		 conversation = new ArrayList<Pair>(); 
                Pair chat = new Pair();
                chat.person = one;
                    chat.message = message;
                conversation.add(chat);
            }

	public Conversation() {

	}
        public void setConversation(User one, String message) {
            
            Pair chat = new Pair();
            chat.person = one;
            chat.message = message;
            //System.out.print(chat.message+ " "+chat.person.getID());
            conversation.add(chat);
//               System.out.println("\nConversation size\n"+conversation.size());
        }
	public void setMessage(String message,int indxPerson ) {
		Pair chat= conversation.get(indxPerson);
                chat.message= message;
	}
	public void setPeople(User person) {
            Pair chat= new Pair();
            chat.person =person; 
            System.out.print("hello setPep");
		conversation.add(chat);
             
	}

	public ArrayList<Pair> getConversation() {
            return conversation; 
	}

	public ArrayList<User> getPeople() {
            ArrayList <User> pep =new ArrayList<User>();
            for(int i=0 ; i< conversation.size();i++){
                pep.add(conversation.get(i).person);
            }
            return pep;
	}

	public User getPerson() {
		return conversation.get(0).person;
        }
	public void removePerson(int indx) {
            Pair chat = conversation.get(indx);
		conversation.remove(chat.person);
	}

	public void removeMessage(int indx) {
            Pair chat =conversation.get(indx);
		conversation.remove(chat.message);
	}
        
}
