package home;

import java.sql.Date;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import jdk.nashorn.internal.ir.RuntimeNode;
public class Notification {

    String kind;
    String date;
    boolean isSeen;
    String text;
    int noti_id;
    int conv_id;
    int friend_req_id;
    int user_id;

    public int getNoti_id() {
        return noti_id;
    }

    public void setNoti_id(int noti_id) {
        this.noti_id = noti_id;
    }

    public int getConv_id() {
        return conv_id;
    }

    public void setConv_id(int conv_id) {
        this.conv_id = conv_id;
    }

    public int getFriend_req_id() {
        return friend_req_id;
    }

    public void setFriend_req_id(int friend_req_id) {
        this.friend_req_id = friend_req_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
    
    public Notification(String k){
        kind=k;
    }
    public Notification() {
        this.kind = null;
        this.date = null;
        this.text = null;
//        System.out.println(date);
    }
public Notification(String kind, String date, String text) {
        this.kind = kind;
        this.date = date;
        this.text = text;
//        System.out.println(date);
    }

    @Override
    public String toString() {
        return "Notification{" + "kind=" + kind + ", date=" + date + ", text=" + text + ", conv_id=" + conv_id + ", friend_req_id=" + friend_req_id + ", user_id=" + user_id + '}';
    }

    
    public Notification(String kind, String text) {
        this.kind = kind;
        SimpleDateFormat d=new SimpleDateFormat();
        date=d.toString();
        this.text = text;
//        System.out.println(date);
    }
    public String getKind() {
            return kind;
    }
    public void setKind(String kind) {

            this.kind = kind;
    }
    public String getDate() {
            return date;
    }
    public void setDate(String date) {
            this.date = date;
    }
    public boolean isSeen() {
            return isSeen;
    }
    public void setSeen(boolean isSeen) {
            this.isSeen = isSeen;
    }
    public String getText() {
            return text;
    }
    public void setText(String text) {
            this.text = text;
    }
    public void insertInDB(){
        String query ="insert into Notification (kind ,date,text) values('"
                + kind+"',"
                + date+",'"
                + text+"'";
        Connection con=null;
        Statement st=null;
        ResultSet rs=null;
        try{
            Class.forName("org.sqlite.JDBC");
            con= DriverManager.getConnection("jdbc:sqlite:OverWorld.sqlite");
            st=con.createStatement();
            rs=st.executeQuery(query);
            System.out.println(rs);
        }catch(Exception ex){
            System.out.println(ex);
        }
    }
	
}
