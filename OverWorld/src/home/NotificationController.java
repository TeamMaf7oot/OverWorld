package home;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import overworld.*;
import signinregister.SiginController;

public class NotificationController {
    ArrayList<Notification> notis=new ArrayList<>();
    public void markAsSeen(int index){
            notis.get(index).setSeen(true);
    }
    public void deleteNotification(int index){////////// delete from data base
            notis.remove(index);
    }
    public ArrayList<Notification> getNotificationfromDB(){
        DBH2 D=new DBH2();
        try{System.out.println("now ok");
            D.open_connection();
        
            ResultSet rs =D.executequery("SELECT * FROM Notification where user_id ="
                    +SiginController.me.getID()+" and is_seen = 'false' and (kind = 'brand' or kind = 'check_in')");
            while(rs.next()){
                System.out.println(rs.getInt("noti_id"));
                notis.add(new Notification(rs.getString("kind"),rs.getString("date"), rs.getString("text")));
            }
            System.out.println("overworld.NotificationController.getNotificationfromDB()--------->");
            
        }catch (SQLException ex) {
            Logger.getLogger(NotificationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            D.close_connection();
        }
        return notis;
    }
    public void goToNotiSource(int index){
        notis.get(index).setSeen(true);
        //ResultSet
    }
}
