package userprofile;

import java.sql.ResultSet;
import java.sql.SQLException;
import place.Location;
import place.CheckIn;
import place.Place;
import place.Location;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import overworld.DBH2;
import overworld.OverWorld;
import place.Location;
import signinregister.SiginController;

public class User {
    private String Name;
    private String email;
    private String password;
    private int id ; 
    private String creditcard;
    private boolean ispre;
    private String passwordcredit;
    private ArrayList<Place> myPlaces;
    private ArrayList<User> friends;
    private ArrayList<String>tastes;
    private ArrayList<CheckIn>CheckIns;
    private ArrayList<Place> savedPlaces;
    private Location location;
    private String question;
    private String answer;
    
    public User(){
        Name = null;
        email = null;
        password = null;
        id = -1;
        creditcard = null;
        ispre = false;
        passwordcredit = null;
        myPlaces = null;
        friends = null;
        tastes = null;
        CheckIns = null;
        savedPlaces = null;
        location = null;
    }
    public User(String Name, String email, String pass, int id, String creditcard, boolean ispre, String passwordcredit, ArrayList<Place> myPlaces, ArrayList<User> friends, ArrayList<String> tastes, ArrayList<CheckIn> CheckIns, ArrayList<Place> savedPlaces, Location location) {
        this.Name = Name;
        this.email = email;
        this.password = pass;
        this.id = id;
        this.creditcard = creditcard;
        this.ispre = ispre;
        this.passwordcredit = passwordcredit;
        this.myPlaces = myPlaces;
        this.friends = friends;
        this.tastes = tastes;
        this.CheckIns = CheckIns;
        this.savedPlaces = savedPlaces;
        this.location = location;
    }
    public User (String email){
        DBH2 db=new DBH2();
        ResultSet rs = null;
        try {System.out.println("userprofile.User.<init>()llllllllllll");
            db.open_connection();
            rs=db.executequery("SELECT * FROM User WHERE email ='" + email+"'");
            if(rs!=null){
                while (rs.next()) {
                    setID(Integer.valueOf(rs.getString("user_id")));
                    setEmail(rs.getString("email"));
                    setCreditcard(rs.getString("creditcard"));

                    ResultSet res = null;
                    tastes = new ArrayList<>();
                    if (rs.getString("taste_id")!=null) {
                        int taste_id = Integer.valueOf(rs.getString("taste_id"));
                        res = db.executequery("SELECT name FROM Taste WHERE taste_id =" + taste_id);
                        while (res.next()) {
                            tastes.add(res.getString("name"));

                        }
                        res.close();
                    }

                    setName(rs.getString("name"));
                    setPass(rs.getString("password"));
                    System.out.println("userprofile.User.<init>(): "+password);
                    setPasswordcredit(rs.getString("passwordcredit"));

                    String ispre = rs.getString("ispre");
                    if (ispre == "true") {
                        setIspre(true);
                    } else {
                        setIspre(false);
                    }
                    setQuestion(rs.getString("question"));
                    setAnswer(rs.getString("answer"));
                    String loc[] = rs.getString("location").split(",");
                    Location location = new Location(Integer.valueOf(loc[0]), Integer.valueOf(loc[1]));
                    setLocation(location);
                }
            }
        } catch (Exception e) {

        } finally {
//            try {
//                rs.close();
                db.close_connection();
//            } catch (SQLException ex) {
//                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
//            }

        }
        UserController uc=new UserController();
        setCheckIns(uc.get_CheckIns());
        
    }
    
    
     public void add_CheckIn(CheckIn checkin) {

        this.add_check_in(checkin);
        DBH2 D = new DBH2();
        ResultSet rs = null;
        try {
            D.open_connection();
            rs = D.executequery("SELECT check_in_id FROM User WHERE user_id =" + id);
            String check_in_id = rs.getString("check_in_id");
            if (check_in_id == null) {
                int max = -1;
                rs = D.executequery("SELECT check_in_id FROM Check_in");
                while (rs.next()) {
                    check_in_id = rs.getString("check_in_id");
                    if (Integer.valueOf(check_in_id) > max) {
                        max = Integer.valueOf(check_in_id);
                    }
                }
                D.executequery("INSERT INTO Check_in (check_in_id ,text ,place_id) VALUES (" + max + ", '" + checkin.getPost() + "' ," + checkin.getPlace().getplace_id() + ")");
                D.executeupdate("UPDATE User SET check_in_id =" + max + " WHERE user_id =" + id);
            } else {
                D.executequery("INSERT INTO Check_in (check_in_id ,text ,place_id) VALUES (" + check_in_id + ", '" + checkin.getPost() + "' ," + checkin.getPlace().getplace_id() + ")");
            }
        } catch (Exception e) {
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                D.close_connection();
            } catch (Exception e) {
            }
        }
    }

//    public User(String email,String pass) throws SQLException{
//        
//        String query="select * from User where email = '"+email+"' and password = '"+password+"'";
//        ResultSet rs=OverWorld.db.executequery(query);
//        System.out.println("3654354564555555555555555555555555555555555555555555555555555");
////User me;
//        rs.next();
//            id = rs.getInt("user_id");
//            Name = rs.getString("name");
//            this.email = email;
//            password = pass;
//            
//            creditcard = rs.getString("creditCard");
//            ispre = rs.getBoolean("isPre");
//            passwordcredit = rs.getString("passwordCredit");
//            myPlaces = null;
//            friends = null;
//            tastes = null;
//            CheckIns = null;
//            savedPlaces=null;
////            query="select * from SavedPlaces where user_id = "+id;
////            rs=DBH.excuteQuery(query);
////            while(rs.next()){
////                savedPlaces.add(new Place());
////                savedPlaces.get(savedPlaces.size()-1).setplace_id(rs.getInt("place_id"));
////            }
//            location = null;
//            
//        
//        
////        try {
//////            if(rs.next()){
//////                setSavedPlaces(rs);
//////            }
//////            else {
//////                rs.close();
////////                return null;
//////            }
////        } catch (SQLException ex) {
////            Logger.getLogger(SiginController.class.getName()).log(Level.SEVERE, null, ex);
////        }
//        
////        return this;
//    }
    
    
    public ArrayList<Place> getSavedPlaces() {
        return savedPlaces;
    }

    public void setSavedPlaces(ArrayList<Place> savedPlaces) {
        this.savedPlaces = savedPlaces;
    }
    public void setSavedPlaces(ResultSet rs) {
        
        //todo
    }
    public Location getLocation() {
            return location;
    }
    public void setID(int id){
        this.id= id;
    }
    public int getID(){
        return id;
    }
    public void setLocation(Location location) {
            this.location = location;
    }
    public String getName() {
            return Name;
    }
    public void setName(String name) {
            Name = name;
    }
    public String getEmail() {
            return email;
    }
    public void setEmail(String email) {
            this.email = email;
    }
    public String getPass() {
            return password;
    }
    public void setPass(String pass) {
            this.password = pass;
    }
    public String getCreditcard() {
            return creditcard;
    }
    public void setCreditcard(String creditcard) {
            this.creditcard = creditcard;
    }
    public boolean isIspre() {
            return ispre;
    }
    public void setIspre(boolean ispre) {
            this.ispre = ispre;
    }
    public String getPasswordcredit() {
            return passwordcredit;
    }
    public void setPasswordcredit(String passwordcredit) {
            this.passwordcredit = passwordcredit;
    }
    public ArrayList<Place> getMyPlaces() {
            return myPlaces;
    }
    public void setMyPlaces(ArrayList<Place> myPlaces) {
            this.myPlaces = myPlaces;
    }
    public ArrayList<User> getFriends() {
            return friends;
    }
    public void setFriends(ArrayList<User> friends) {
            this.friends = friends;
    }
    public ArrayList<String> getTastes() {
            return tastes;
    }
    public void setTastes(ArrayList<String> tastes) {
            this.tastes = tastes;
    }
    public ArrayList<CheckIn> getCheckIns() {
            return CheckIns;
    }
    public void setCheckIns(ArrayList<CheckIn> checkIns) {
            CheckIns = checkIns;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
    
    public void addPremUser(String username, String email ,String Password,String CreditCard,
                    Boolean ispre,String CreditPass) throws SQLException{
        addNormalUser(username, email ,Password, ispre);
        UserController uc=new UserController();
        uc.verifyCreditcard(CreditPass, Password);
        this.creditcard = CreditCard;
        this.passwordcredit = CreditPass;
        
    }
    public void addNormalUser(String username, String email ,String Password, Boolean ispre){
        this.Name = username;
        this.email = email;
        this.password = Password;
        this.creditcard = "";
        this.ispre = ispre;
        this.passwordcredit = "";
        this.myPlaces = new ArrayList<>();
        this.friends = new ArrayList<>();
        this.tastes = new ArrayList<>();
        this.CheckIns = new ArrayList<>();
        this.savedPlaces = new ArrayList<>();
        this.location = new Location(0, 0);
    }
    public void add_check_in(CheckIn checkin)
    {
    CheckIns.add(checkin);
    }

    @Override
    public String toString() {
        return "User{" + "Name=" + Name + ", email=" + email + ", password=" + password + ", id=" + id + ", creditcard=" + creditcard + ", ispre=" + ispre + ", passwordcredit=" + passwordcredit + ", location=" + location + ", question=" + question + ", answer=" + answer + '}';
    }
	
}
