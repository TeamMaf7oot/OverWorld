/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userprofile;


import place.PlaceController;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import overworld.*;
import place.CheckIn;
import place.Location;

/**
 *
 * @author Dell2015
 */
public class UserController {

    ArrayList<PlaceController> homeplacelist;
    User me;
    public UserController(){
        me=signinregister.SiginController.me;
    }
    public void Upgrade(String Credit, String passwordCredit) throws SQLException {
        
        DBH2 D=new DBH2();
        try{
        
            D.open_connection();
            D.executeupdate("UPDATE User SET creditcard = '" + Credit + "' , passwordcredit = '" + passwordCredit + "',ispre = 'true' WHERE email = '" + me.getEmail() + "' ;");
        }catch(Exception e)
        {
        
        }finally
        {
        //D.close_connection();
        }
        
    }

    public boolean verifyCreditcard(String Credit, String password) throws SQLException {
        DBH2 D=new DBH2();
        ResultSet rs=null;
        String passwordcredit="";
        try{
        D.open_connection();
        rs=D.executequery("Select password FROM Bank WHERE creditcard ='" + Credit + "';");
        passwordcredit= rs.getString("password");
        System.out.println(password);
        }catch(Exception e)
        {
        
        }finally
        {
            if(rs!=null)
                rs.close();
        D.close_connection();
        }
        if (password.equals(passwordcredit)) {
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<CheckIn> get_CheckIns() {
        ArrayList<CheckIn> checkins = new ArrayList<CheckIn>();
        ArrayList<String>place_id=new ArrayList<String>();
        PlaceController P = new PlaceController();
        CheckIn C = new CheckIn();
        DBH2 D = new DBH2();
        ResultSet rs = null;
        try {
            D.open_connection();
            rs = D.executequery("SELECT check_in_id FROM User WHERE user_id =" + me.getID());
            String check_in_id=rs.getString("check_in_id");
            rs=D.executequery("SELECT text ,place_id FROM Check_in WHERE check_in_id ="+check_in_id);
            while (rs.next()) {
                C.setPost(rs.getString("text"));
                C.setPlace(P.getPlace(Integer.valueOf(rs.getString("place_id"))));
                checkins.add(C);
                C=new CheckIn ();
            }
        } catch (Exception e) {

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            D.close_connection();
        }
        return checkins;
    }

    
    public User get_user(String email) {
        me = new User();
        DBH2 D=new DBH2();
        ResultSet rs = null;
        try {
            D.open_connection();
            rs = D.executequery("SELECT * FROM User WHERE email ='" + email+"'");
            while (rs.next()) {
                me.setID(Integer.valueOf(rs.getString("user_id")));
                me.setEmail(rs.getString("email"));
                me.setCreditcard(rs.getString("creditcard"));
                String loc[] = rs.getString("location").split(",");
                ResultSet res = null;
                ArrayList<String> tastes = new ArrayList<String>();
                if (rs.getString("taste_id") != null) {
                    int taste_id = Integer.valueOf(rs.getString("taste_id"));
                    res = D.executequery("SELECT name FROM Taste WHERE taste_id =" + taste_id);
                    while (res.next()) {
                        tastes.add(res.getString("name"));
                       
                    }
                }
                res.close();
                me.setTastes(tastes);
                me.setName(rs.getString("name"));
                me.setPass(rs.getString("password"));
                me.setPasswordcredit(rs.getString("passwordcredit"));
                Location location = new Location(Integer.valueOf(loc[0]), Integer.valueOf(loc[1]));
                me.setLocation(location);
                String ispre = rs.getString("ispre");
                if (ispre.equals( "true")) {
                    me.setIspre(true);
                } else {
                    me.setIspre(false);
                }
                me.setQuestion(rs.getString("question"));
                me.setAnswer("answer");
            }
        } catch (Exception e) {

        } finally {
            try {
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            }
            D.close_connection();

        }
        me.setCheckIns(get_CheckIns());
        return me;
    }

  
    public ArrayList<CheckIn> return_checkIn()
    {
    return me.getCheckIns();
    }
    public Location get_location()
    {
    return me.getLocation();
    }
    public ArrayList<String> get_tastes()
    {
    return me.getTastes();
    }
    public boolean get_ispre()
    {
    return me.isIspre();
    }
    public int get_id() {
        return me.getID();
    }
}
