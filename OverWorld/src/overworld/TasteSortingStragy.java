package overworld;

import place.Place;
import java.util.ArrayList;
import place.CheckIn;
import place.Place;

public class TasteSortingStragy extends SortingStragy {

    ArrayList<String> tastes = new ArrayList<String>();

    public TasteSortingStragy(ArrayList<CheckIn> checkIns, ArrayList<String> tastes) {
        super(checkIns);
        this.tastes = tastes;
        Sort();
    }

    @Override
    public void Sort() {
       
        
        ArrayList<Integer> counter = new ArrayList<Integer>();
        
        for (int i = 0; i < CheckIns.size(); i++) {
            counter.add(find(tastes, CheckIns.get(i).getPlace().getTastes()));
        }
        for (int i = 0; i < counter.size(); i++) {
            for (int j = (i + 1); j < counter.size(); j++) {
                if (counter.get(j) > counter.get(i)) {
                    CheckIn c=CheckIns.get(i);
                    CheckIns.set(i, CheckIns.get(j));
                    CheckIns.set(j, c);
                }
            }
        }
        

    }

    public int find(ArrayList<String> taste1, ArrayList<String> taste2) {
        int count = 0;
        for (int i = 0; i < taste2.size(); i++)//taste user
        {
            for (int j = 0; j < taste1.size(); j++) {
                if (taste2.get(i).equals(taste1.get(j))) {
                    count++;
                }
            }
        }
        return count;
    }

}
