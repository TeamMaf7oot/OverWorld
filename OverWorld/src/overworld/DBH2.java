/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package overworld;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hend
 */
public class DBH2 {
    private   Connection con;
    private   Statement st;
    private   ResultSet rs;
    public   void open_connection() 
    {
        con=null;
         st=null;
         rs=null;
        try {
            Class.forName("org.sqlite.JDBC");
            con= DriverManager.getConnection("jdbc:sqlite:OverWorld.sqlite");
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DBH2.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        
           
    }
    public   void executeupdate(String query)
    {
        try {
            st=con.createStatement();
            st.executeUpdate(query);
        } catch (SQLException ex) {
            Logger.getLogger(DBH2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public   ResultSet executequery(String query)
    {
        try {
            st=con.createStatement();
            rs=st.executeQuery(query);
        } catch (SQLException ex) {
            Logger.getLogger(DBH2.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
     public void close_connection()
    {
        try {
            con.close();
//            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBH2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    /*public static ResultSet excuteQuery(String query){
        Connection con=null;
        Statement st=null;
        ResultSet rs=null;
        
        try {
            Class.forName("org.sqlite.JDBC");
            con= DriverManager.getConnection("jdbc:sqlite:OverWorld.sqlite");
            st=con.createStatement();
            rs=st.executeQuery(query);
            System.out.println("okay");
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DBH.class.getName()).log(Level.SEVERE, null, ex);
        }
//        try {
//            if(rs.next()){
//                System.out.println("overworld.DBH.excuteQuery() okay");
//                return rs;
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(DBH.class.getName()).log(Level.SEVERE, null, ex);
//        }
        return rs;
    }*/
    
}
