package place;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Place {
	private String placeName;
	private String Owner;
	private Location location;
	private ArrayList<String >tastes;
	private ArrayList<Tip>tips;
	private int rate;
	private ArrayList<CheckIn>listCheckIn;
	private int numberOfSaveProcess;
	private int place_id;
        private int counter;
	
	public Place()
	{
            tastes=new ArrayList<String >();
            tips=new ArrayList<Tip>();
            listCheckIn=new ArrayList<CheckIn>();
		
	}
	public Place(String placeName, String Owner, Location location, ArrayList<String> tastes,
			Tip tip, int rate, int numberOfSaveProcess) {
		this.placeName = placeName;
		this.Owner = Owner;
		this.location = location;
		this.tastes = tastes;
                tips=new ArrayList<Tip>();
		tips.add(tip);
		this.rate = rate;
                counter=0;
		this.numberOfSaveProcess = numberOfSaveProcess;
	}
        public int getcounter()
        {
        return counter;
        }
        public void setcounter(int count)
        {
        counter=count;
        }
        public void setplace_id(int id)
        {
        place_id=id;
        }
        public int getplace_id()
        {
        return place_id;
        }
        public void setTaste(String text)        
        {
            tastes.add(text);
        }
	public String getPlaceName() {
		return placeName;
	}
	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}
	public String getOwner() {
		return Owner;
	}
	public void setOwner(String owner) {
		Owner = owner;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public ArrayList<String> getTastes() {
		return tastes;
	}
	public void setTastes(ArrayList<String> tastes) {
		this.tastes = tastes;
	}
	public ArrayList<Tip> getTips() {
		return tips;
	}
	public void setTips(ArrayList<Tip> tips) {
		this.tips = tips;
	}
	public int getRate() {
		return rate;
	}
	public void setRate(int rate) {
		this.rate = rate;
	}
	public ArrayList<CheckIn> getListCheckIn() {
		return listCheckIn;
	}
	public void setListCheckIn(ArrayList<CheckIn> listCheckIn) {
		this.listCheckIn = listCheckIn;
	}
	public int getNumberOfSaveProcess() {
		return numberOfSaveProcess;
	}
	public void setNumberOfSaveProcess(int numberOfSaveProcess) {
		this.numberOfSaveProcess = numberOfSaveProcess;
	}
        public void set_in_database(ArrayList<String>taste) throws SQLException
        {
        String querey;//="SELECT tip_id FROM Tips";
        overworld.DBH db=new overworld.DBH();
        ResultSet rs=db.excuteQuery("SELECT tip_id FROM Tips");
        String id = null;
        int max=-1;
        while(rs.next())
        {
        id=rs.getString("tip_id");
        if(Integer.valueOf(id)>max)
            max=Integer.valueOf(id);
        }
        int tip_id=max;
        tip_id++;
        querey="INSERT INTO Tips (tip_id ,likes ,text ) VALUES ("+tip_id+",0,'"+tips.get(0).getTips()+"');";
        rs=db.excuteQuery(querey);
        
        querey="SELECT taste_id FROM Taste";
        rs=db.excuteQuery(querey);
        max=-1;
        while(rs.next())
        {
             id=rs.getString("taste_id");
             if(Integer.valueOf(id)>max)
            max=Integer.valueOf(id);
        }
        int taste_id=max;
        taste_id++;
        for(int i=0;i<taste.size();i++)
        {
        querey="INSERT INTO Taste (taste_id ,name ) VALUES ("+taste_id+" , '"+taste.get(i)+"');";
        db.excuteQuery(querey);
        }
        
        
        querey="SELECT place_id FROM place";
        rs=db.excuteQuery(querey);
        while(rs.next())
        {
             id=rs.getString("place_id");
        }
        int place_id=Integer.valueOf(id);
        place_id++;
        querey="INSERT INTO place (place_id ,tip_id ,taste_id ,place_name ,numOfCheckIn ,rate ,numOfSaveProcess ,Location_ID ,counter ,owner) VALUES ("+place_id+" , "+tip_id+" ,"+taste_id+",'"+placeName+"', 0, 0, 0,'"+location.toString()+"', 0 ,'"+Owner+"');";
        db.excuteQuery(querey);
        }
	

}
