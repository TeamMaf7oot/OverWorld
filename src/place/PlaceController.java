package place;

import place.Place;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PlaceController {

    private Place place;

    public void addPlace(String name, String tip, String owner, int lat, int long_, ArrayList<String> taste, int rate, int numofprocess) throws SQLException {
        Tip Tips = new Tip(0, tip);
        Location loc = new Location(lat, long_);
        String location = Double.toString(lat) + "," + Double.toString(long_);
        place = new Place(name, owner, loc, taste, Tips, rate, numofprocess);

        place.set_in_database(taste);

    }

    public void addTip(Tip tip) ///later not in this phase 
    {

    }

    public void addTaste(String text) throws SQLException {
        place = new Place();
        place.setplace_id(1);
        place.setTaste(text);
        String querey = "SELECT taste_id FROM place WHERE place_id =" + place.getplace_id();
        overworld.DBH db = new overworld.DBH();
        ResultSet rs = db.excuteQuery(querey);
        String taste_id = rs.getString("taste_id");
        querey = "INSERT INTO Taste ( taste_id , name ) VALUES (" + taste_id + ", '" + text + "')";
        db.excuteQuery(querey);
    }

    public void deleteTaste(int index) {

    }

    /*public void UpdateRate(int rate_value) throws SQLException, InterruptedException
	{
            int rate=place.getRate();
            int counter=place.getcounter();
            rate*=counter;
            counter++;
            place.setcounter(counter);
            rate+=rate_value;
            rate/=counter;
            place.setRate(rate);
            int id=place.getplace_id();
            System.out.println("rate "+place.getRate()+" counter "+place.getcounter()+" id "+id);
            String querey="UPDATE place SET rate = "+Integer.toString(rate)+" , counter = "+Integer.toString(counter )+" WHERE place_id = "+Integer.toString(id)+";";
            DBH db=new DBH();
            db.excuteQuery(querey);
       
	}*/
    public void addCheckIn(String text) throws SQLException {
        CheckIn checkin = new CheckIn(text, place);
        String querey = "SELECT check_in_id FROM place WHERE place_id = " + Integer.toString(place.getplace_id()) + ";";
        overworld.DBH db = new overworld.DBH();
        ResultSet rs = db.excuteQuery(querey);
        String id = null;
        id = rs.getString("check_in_id");
        int check_in_id;
        if (id == null) {
            querey = "SELECT check_in_id FROM Check_in";

            rs = db.excuteQuery(querey);
            int max = -1;
            while (rs.next()) {
                id = rs.getString("check_in_id");
                if (Integer.valueOf(id) > max) {
                    max = Integer.valueOf(id);
                }
            }
            check_in_id = max;
            check_in_id++;
        } else {
            check_in_id = Integer.valueOf(id);
        }

        querey = "INSERT INTO Check_in (check_in_id ,text ,likes ) VALUES ( " + check_in_id + " ,'" + text + "' ,0);";
        db.excuteQuery(querey);
        if (id == null) {
            querey = "UPDATE place SET check_in_id = " + check_in_id + " WHERE place_id=" + place.getplace_id() + ";";
            db.excuteQuery(querey);
        }
    }

    public void deleteTip(int index) {

    }

    public void UpdateTip(int index, String text) {

    }

    public void UpdateCheckIn(int index, String text) {

    }

    public void deleteCheckIn(int index) {

    }

    public void UpdateNumOfSaveProcess() {

    }

    public void UpdateNumLikeTip(int index, int number) {

    }

    /*public void getPlace(int place_id) throws SQLException
	{
        DBH db=new DBH();
        ResultSet rs=null;
        ResultSet r=null;
     
        String querey="SELECT * FROM place WHERE place_id = "+place_id+";";
        rs=db.excuteQuery(querey);
        String name=rs.getString("place_name");
        String numofCheckin=rs.getString("numOfCheckIn");
        String owner=rs.getString("owner");
        String count=rs.getString("counter");
        String rate=rs.getString("rate");
        String numOfSaveProcess=rs.getString("numOfSaveProcess");
        String counter=rs.getString("counter");
        String location_id=rs.getString("location_id");
        String taste_id=rs.getString("taste_id");
        String tip_id=rs.getString("tip_id");
        String check_in_id=rs.getString("check_in_id");
        ArrayList<String >tastes=new ArrayList<String>();
        querey="SELECT name FROM Taste WHERE taste_id = "+taste_id+";";
        rs=db.excuteQuery(querey);
        while(rs.next())
        {
        tastes.add(rs.getString("name"));
        }
        ArrayList<Tip>tips=new ArrayList<Tip>();
        querey="SELECT * FROM Tips WHERE tip_id = "+tip_id+";";
        rs=db.excuteQuery(querey);
        while(rs.next())
        {
            String likes=rs.getString("likes");
            String text=rs.getString("text");
            Tip t=new Tip(Integer.valueOf(likes),text); 
            tips.add(t);
        }
        ArrayList<CheckIn>checkins=new ArrayList<CheckIn>();
        CheckIn C;
        if(check_in_id!=null)
        {
        querey="SELECT * FROM Check_in WHERE check_in_id = "+check_in_id+";";
        rs=db.excuteQuery(querey);
        while(rs.next())
        {
        String likes=rs.getString("likes");
        String text=rs.getString("text");
        String comments_id=rs.getString("comments_id");
        ArrayList<String>comment=new ArrayList<String>();
        if(comments_id!=null)
        {
        querey="SELECT * FROM comment WHERE comments_id = "+comments_id+";";
        r=db.excuteQuery(querey);
        while(r.next())
        {
        comment.add(r.getString("text"));
        }
        }
        C=new CheckIn();
        C.setComment(comment);
        C.setLike(Integer.valueOf(likes));
        C.setPost(text);
        checkins.add(C);
        }
        }
        String loc[]=location_id.split(",");
        int lat=Integer.valueOf(loc[0]);
        int long_=Integer.valueOf(loc[1]);
        Location location=new Location(lat,long_);
       place =new Place ();
       place.setListCheckIn(checkins);
       place.setLocation(location);
       place.setNumberOfSaveProcess(Integer.valueOf(numOfSaveProcess));
       place.setPlaceName(name);
       place.setRate(Integer.valueOf(rate));
       place.setTastes(tastes);
       place.setTips(tips);
       place.setplace_id(place_id);
       place.setcounter(Integer.valueOf(count));
       place.setOwner(owner);

       for(int i=0;i<checkins.size();i++)
           checkins.get(i).setPlace(place);
	}*/
    public void addLikeToCheckIn(int index) {

    }

    public void deleteLikeToCheckIn(int index) {

    }

    public void addCommentOfToCheckIn(String text) {

    }

    public void deleteCommentOfToCheckIn(int index) {

    }

    public void addNotiOfCheckIntoFriends() {

    }

    public void getNearbyPlaces() {

    }

    public int get_rate() {
        return place.getRate();
    }

    public String get_name() {
        return place.getPlaceName();
    }

    public Tip get_tip() {
        return place.getTips().get(0);
    }

    public ArrayList<CheckIn> get_check_in() {
        return place.getListCheckIn();
    }

    public ArrayList<String> get_tastes() {
        return place.getTastes();
    }

    public void getPlace(int place_id) {
        Connection conn = null;
        ResultSet rs = null;

        try {
            // DBH db=new DBH();
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Dell2015\\Desktop\\Project_SE\\OverWorld\\OverWorld.sqlite");
            Statement stat = conn.createStatement();
            rs = stat.executeQuery("SELECT * FROM place WHERE place_id = " + place_id + ";");

            //String querey="SELECT * FROM place WHERE place_id = "+place_id+";";
            // rs=db.excuteQuery(querey);
            String name = rs.getString("place_name");
            String numofCheckin = rs.getString("numOfCheckIn");
            String owner = rs.getString("owner");
            String count = rs.getString("counter");
            String rate = rs.getString("rate");
            String numOfSaveProcess = rs.getString("numOfSaveProcess");
            String counter = rs.getString("counter");
            String location_id = rs.getString("location_id");
            String taste_id = rs.getString("taste_id");
            String tip_id = rs.getString("tip_id");
            String check_in_id = rs.getString("check_in_id");
            ArrayList<String> tastes = new ArrayList<String>();
            rs.close();
            rs = null;

//        Class.forName("org.sqlite.JDBC");
//         conn = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Dell2015\\Desktop\\Project_SE\\OverWorld\\OverWorld.sqlite");
//         stat = conn.createStatement();
            rs = stat.executeQuery("SELECT name FROM Taste WHERE taste_id = " + taste_id + ";");
            //querey="SELECT name FROM Taste WHERE taste_id = "+taste_id+";";
            // rs=db.excuteQuery(querey);
            while (rs.next()) {
                tastes.add(rs.getString("name"));
            }
            rs.close();
            rs = null;

            ArrayList<Tip> tips = new ArrayList<Tip>();
//         Class.forName("org.sqlite.JDBC");
//         conn = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Dell2015\\Desktop\\Project_SE\\OverWorld\\OverWorld.sqlite");
//         stat = conn.createStatement();
            rs = stat.executeQuery("SELECT * FROM Tips WHERE tip_id = " + tip_id + ";");
            // querey="SELECT * FROM Tips WHERE tip_id = "+tip_id+";";
            //rs=db.excuteQuery(querey);
            while (rs.next()) {
                String likes = rs.getString("likes");
                String text = rs.getString("text");
                Tip t = new Tip(Integer.valueOf(likes), text);
                tips.add(t);
            }

            rs.close();
            rs = null;
            ArrayList<CheckIn> checkins = new ArrayList<CheckIn>();
            CheckIn C;
            if (check_in_id != null) {
//            
//            Class.forName("org.sqlite.JDBC");
//         conn = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Dell2015\\Desktop\\Project_SE\\OverWorld\\OverWorld.sqlite");
                stat = conn.createStatement();
                rs = stat.executeQuery("SELECT * FROM Check_in WHERE check_in_id = " + check_in_id + ";");
                //querey="SELECT * FROM Check_in WHERE check_in_id = "+check_in_id+";";
                //rs=db.excuteQuery(querey);
                while (rs.next()) {
                    String likes = rs.getString("likes");
                    String text = rs.getString("text");
                    /*String comments_id=rs.getString("comments_id");
        ArrayList<String>comment=new ArrayList<String>();
        if(comments_id!=null)
        {
           Class.forName("org.sqlite.JDBC");
         conn = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Dell2015\\Desktop\\Project_SE\\OverWorld\\OverWorld.sqlite");
         stat = conn.createStatement();
        ResultSet r=stat.executeQuery("SELECT * FROM comment WHERE comments_id = "+comments_id+";"); 
        //querey="SELECT * FROM comment WHERE comments_id = "+comments_id+";";
       // r=db.excuteQuery(querey);
        while(r.next())
        {
        comment.add(r.getString("text"));
        }
        r.close();
        }*/
                    C = new CheckIn();
                    // C.setComment(comment);
                    C.setLike(Integer.valueOf(likes));
                    C.setPost(text);
                    checkins.add(C);
                }
            }
            rs.close();
            rs = null;

            String loc[] = location_id.split(",");
            int lat = Integer.valueOf(loc[0]);
            int long_ = Integer.valueOf(loc[1]);
            Location location = new Location(lat, long_);
            place = new Place();
            place.setListCheckIn(checkins);
            place.setLocation(location);
            place.setNumberOfSaveProcess(Integer.valueOf(numOfSaveProcess));
            place.setPlaceName(name);
            place.setRate(Integer.valueOf(rate));
            place.setTastes(tastes);
            place.setTips(tips);
            place.setplace_id(place_id);
            place.setcounter(Integer.valueOf(count));
            place.setOwner(owner);

            for (int i = 0; i < checkins.size(); i++) {
                checkins.get(i).setPlace(place);
            }

        } catch (Exception e) {

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(PlaceController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void UpdateRate(int rate_value) {

        int rate = place.getRate();
        int counter = place.getcounter();
        rate *= counter;
        counter++;
        place.setcounter(counter);
        rate += rate_value;
        rate /= counter;
        place.setRate(rate);
        int id = place.getplace_id();
        System.out.println("rate " + place.getRate() + " counter " + place.getcounter() + " id " + id);
        Connection conn = null;
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Dell2015\\Desktop\\Project_SE\\OverWorld\\OverWorld.sqlite");

            Statement stat = conn.createStatement();
            int cnt = stat.executeUpdate("UPDATE place SET rate = " + rate + " , counter = " + counter + " WHERE place_id = " + id + ";");

        } catch (Exception e) {

        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(PlaceController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

//String querey="UPDATE place SET rate = "+Integer.toString(rate)+" , counter = "+Integer.toString(counter )+" WHERE place_id = "+Integer.toString(id)+";";
        // DBH db=new DBH();
        //db.excuteQuery(querey);
    }

}
