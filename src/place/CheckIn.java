package place;

import place.Place;
import java.util.ArrayList;

public class CheckIn {
	
	private int like;
	private ArrayList<String>comment;
	private String post;
	private Place place ;
	
	
	public CheckIn()
        {
        comment=new ArrayList<String>();
        }
	public CheckIn( String post, Place place) {
		//super();
		like = 0;
		comment = new ArrayList<String>();
		this.post = post;
		this.place = place;
	}
        public void setLike(int x)
        {
        like=x;
        }
	public int getLike() {
		return like;
	}
	public void setLike() {
		like ++;
	}
	public ArrayList<String> getComment() {
		return comment;
	}
	public void setComment(ArrayList<String> comment) {
		this.comment = comment;
	}
	public String getPost() {
		return post;
	}
	public void setPost(String post) {
		this.post = post;
	}
	public Place getPlace() {
		return place;
	}
	public void setPlace(Place place) {
		this.place = place;
	}

	
	
}
