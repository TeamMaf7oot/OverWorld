package overworld;

import place.*;
import java.util.ArrayList;


public class NearSortingStragy extends SortingStragy{

    Location location_user;
    public NearSortingStragy(ArrayList<place.Place> places,Location location_user) {
        super(places);
        this.location_user=location_user;
    }
    

	
    @Override
    public void Sort() {
        for(int i=0;i<places.size();i++)
        {
            int nearlat=(int) Math.abs((places.get(i).getLocation().getLat())-(location_user.getLat()));
            int nearlong=(int) Math.abs((places.get(i).getLocation().getLong_())-(location_user.getLat()));
            for(int j=i;j<places.size();j++)
            {
                int errorlat =(int) Math.abs((places.get(j).getLocation().getLat())-(location_user.getLat()));
                int errorlong=(int) Math.abs((places.get(j).getLocation().getLong_())-(location_user.getLong_()));
                if(errorlat<nearlat&&errorlong<nearlong)
                {
                        Place P=places.get(i);
                        places.set(i, places.get(j));
                        places.set(j, P);
                }
            }
        }

    }

}
