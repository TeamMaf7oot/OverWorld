package overworld;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import place.Place;

public class RatingSoringStragy extends SortingStragy {

    public RatingSoringStragy(ArrayList<Place> places) {
        super(places);
    }

    @Override
    public void Sort() {

        for (int i = 0; i < places.size(); i++) {
            for (int j = (i + 1); j < places.size(); j++) {
                if (places.get(i).getRate() > places.get(j).getRate()) {
                    Place P = places.get(i);
                    places.set(i, places.get(j));
                    places.set(j, P);
                }
            }
        }
    }

}
