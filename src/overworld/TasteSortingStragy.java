package overworld;

import java.util.ArrayList;
import place.Place;

public class TasteSortingStragy extends SortingStragy {

    ArrayList<String> tastes = new ArrayList<String>();

    public TasteSortingStragy(ArrayList<Place> places, ArrayList<String> tastes) {
        super(places);
        this.tastes = tastes;
    }

    @Override
    public void Sort() {
        //ArrayList<String> tastes=new ArrayList<String>();
        //tastes.addAll(user.getTastes());
        ArrayList<Integer> counter = new ArrayList<Integer>();

        for (int i = 0; i < places.size(); i++) {
            counter.add(find(tastes, places.get(i).getTastes()));
        }
        for (int i = 0; i < counter.size(); i++) {
            for (int j = (i + 1); j < counter.size(); j++) {
                if (counter.get(i) > counter.get(j)) {
                    Place P = places.get(i);
                    places.set(i, places.get(j));
                    places.set(j, P);
                }
            }
        }
        

    }

    public int find(ArrayList<String> taste1, ArrayList<String> taste2) {
        int count = 0;
        for (int i = 0; i < taste2.size(); i++)//taste user
        {
            for (int j = 0; j < taste1.size(); j++) {
                if (taste1.get(i).equals(taste2.get(j))) {
                    count++;
                }
            }
        }
        return count;
    }

}
