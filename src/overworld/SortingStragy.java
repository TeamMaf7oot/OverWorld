package overworld;

import java.util.ArrayList;
import place.*;

public abstract class SortingStragy {
	
	protected ArrayList<CheckIn>CheckIns;
	
        ArrayList<Place> places=new ArrayList<Place>();
        public SortingStragy(ArrayList<Place> places)
        {
            this.places=places;
        }
        public ArrayList<CheckIn> getCheckIns()
        {
        return CheckIns;
        }
	public abstract void Sort();

}
