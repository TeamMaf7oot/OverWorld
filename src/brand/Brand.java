/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brand;

import place.Place;
import java.util.ArrayList;
import userprofile.User;

/**
 *
 * @author Dell2015
 */
public class Brand {
    
    private ArrayList<Link>links;
    private  ArrayList<Place>places;
    private ArrayList<String>tastes;
    private ArrayList<User>followers; 
    private int brand_id;
    private User owner;

    
    public Brand()
    {
    
        links=new ArrayList<Link>();
        places=new ArrayList<Place>();
        tastes=new ArrayList<String>();
        followers=new ArrayList<User>();
    }
    public Brand(ArrayList<Link> links, ArrayList<Place> places,ArrayList<String>tastes,User owner) {
        links=new ArrayList<Link>();
        places=new ArrayList<Place>();
        tastes=new ArrayList<String>();
        followers=new ArrayList<User>();
        this.tastes=tastes;
        this.links = links;
        this.places = places;
        this.owner=owner;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public ArrayList<Link> getLinks() {
        return links;
    }

    public ArrayList<Place> getPlaces() {
        return places;
    }

    public ArrayList<User> getFollowers() {
        return followers;
    }

    public void setLinks(ArrayList<Link> links) {
        this.links = links;
    }

    public void setPlaces(ArrayList<Place> places) {
        this.places = places;
    }

    public void setFollowers(ArrayList<User> followers) {
        this.followers = followers;
    }
    
    public ArrayList<String> getTastes() {
        return tastes;
    }
    
    public void set_tatse(String taste)
    {
    tastes.add(taste);
    }

    public int getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(int brand_id) {
        this.brand_id = brand_id;
    }

    public void setTastes(ArrayList<String> tastes) {
        this.tastes = tastes;
    }
    
    public void add_in_database()
    {
    
    }

    
    
}
