/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brand;

/**
 *
 * @author Dell2015
 */
public class Link {
    private String link;
    private String name;

    private int link_id;
    
    public Link(String link, String name) {
        this.link = link;
        this.name = name;
    }

    public void setLink_id(int link_id) {
        this.link_id = link_id;
    }

    public String getLink() {
        return link;
    }

    public String getName() {
        return name;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    
}
