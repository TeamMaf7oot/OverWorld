package conversation;


import userprofile.User;
import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import home.Notification ; 
public class ConversationController {
	 private User sender ;
         static int indxConversation; 
	private ArrayList<Conversation> inbox;
	private Conversation conv;// to notify
	private ConversationDBH databaseHandler = new ConversationDBH(); 
	public ConversationController(){
//		databaseHandler = new ConversationDBH();
	}
	public ConversationController(User one, String mess ) {
		conv = new Conversation(one,mess);
		databaseHandler.insert(mess,indxConversation++,0,sender);
	}

	public ConversationController(ArrayList<User> people, ArrayList<String>message) {
		conv = new Conversation(people, message);
                if(message.size() < 0 )databaseHandler.insert("", indxConversation++,0, sender);
                else {
                    for(int i =0 ; i < message.size(); i++){
                        databaseHandler.insert(message.get(i), indxConversation,i, sender);
                    }
                    indxConversation++;
                }
                databaseHandler.setNoti(indxConversation, message.get(0));
	}

	public void addMessage(String message, int indxCov) {
                Conversation conv = databaseHandler.selectConversation(indxCov);
                // added message to conversation in datbase
                if(conv.getConversation().size() > 0)
                    databaseHandler.insert(message,indxCov,conv.getConversation().size(), sender);
                else 
		databaseHandler.update(indxCov,message, sender);
                 databaseHandler.setNoti(indxCov, message);
	}

	public void addPeople(User one, int indxConv) {
		Conversation conv = databaseHandler.selectConversation(indxConv);
// get conversation indxConv from database and put it in conv;
		if ((conv.getPeople()).size() > 1){
			databaseHandler.insert("", indxConv, 0, one);
		}
		else { 
			ArrayList<User> pp = new ArrayList<User>(); 
			pp.add(one);
			pp.add(conv.getPerson());
                        ArrayList<String>message = new ArrayList<String>();
			Conversation conversation = new Conversation(pp,message);
			databaseHandler.insert("", 0,indxConv++, sender);
		}
	}

	public ArrayList<Conversation> getInbox() {
		inbox = databaseHandler.select();
		return inbox;
	}

	void deletePeople(int indx, int indxConv) {
		try {
			//delete conversation from the User ;
		} catch (Exception e) {
			e.printStackTrace();// null pointer
		}
	}
//	public Notification getNotification() {// which Notification ?????????????
//
//		Notification noti = new Notification();
//		// get noti from database
//		return noti;
//	}
	public void deleteMessage(int indxConv, int indxMess) {
                    Conversation convers = new Conversation();
                    convers = databaseHandler.selectConversation(indxConv);
                    databaseHandler.deletMessage(indxConv,indxMess, sender);
	}

	public Conversation getConversation(int indxConv) {
		Conversation convers = new Conversation();
                convers = databaseHandler.selectConversation(indxConv);
		return convers;
        }
	public void deleteConversation(int indxConversation){
		 databaseHandler.delete(indxConversation);
	}
}
