package conversation;
import userprofile.User;
import java.util.ArrayList;


/**
 * 
 */

/**
 * @author aashmawy
 *
 */
class Pair {
	public String message;
	public User person;    
}

public class Conversation {
	// add to database
	/**
	 * 
	 */
	private ArrayList<Pair> conversation;

	public Conversation(ArrayList<User> person, ArrayList<String>message) {
            conversation = new ArrayList<Pair>(); 
            for(int i=0 ;i < person.size(); i++){
                Pair chat = new Pair();
                chat.person = person.get(i);
                if(message.size() > i )
                    chat.message = message.get(i);
                else chat.message = "";
                conversation.add(chat);
            }
	}

	public Conversation(User one, String message) {
		 conversation = new ArrayList<Pair>(); 
                Pair chat = new Pair();
                chat.person = one;
                    chat.message = message;
                conversation.add(chat);
            }

	public Conversation() {

	}
        public void setConversation(User one, String message) {
            Pair chat= new Pair();
            chat.person = one;
            chat.message = message;
            conversation.add(chat);
        }
	public void setMessage(String message,int indxPerson ) {
		Pair chat= conversation.get(indxPerson);
                chat.message= message;
	}
	public void setPeople(User person) {
            Pair chat= new Pair();
            chat.person =person; 
		this.conversation.add(chat);
	}

	public ArrayList<Pair> getConversation() {
            return conversation; 
	}

	public ArrayList<User> getPeople() {
            ArrayList <User> pep =new ArrayList<User>();
            for(int i=0 ; i< conversation.size();i++){
                pep.add(conversation.get(i).person);
            }
            return pep;
	}

	public User getPerson() {
		return conversation.get(0).person;
        }
	public void removePerson(int indx) {
            Pair chat = conversation.get(indx);
		conversation.remove(chat.person);
	}

	public void removeMessage(int indx) {
            Pair chat =conversation.get(indx);
		conversation.remove(chat.message);
	}

}
