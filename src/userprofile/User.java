package userprofile;

import place.CheckIn;
import place.Place;
import place.Location;
import java.util.ArrayList;
import place.Location;

public class User {
	private String Name;
	private String email;
	private String pass;
        private int id ; 
	private String creditcard;
	private boolean ispre;
	private String passwordcredit;
	private ArrayList<Place> myPlaces;
	private ArrayList<User> friends;
	private ArrayList<String>tastes;
	private ArrayList<CheckIn>CheckIns;
	private Location location;
	public Location getLocation() {
		return location;
	}
        public void setID(int id){
            this.id= id;
        }
        public int getID(){
            return id;
        }
	public void setLocation(Location location) {
		this.location = location;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getCreditcard() {
		return creditcard;
	}
	public void setCreditcard(String creditcard) {
		this.creditcard = creditcard;
	}
	public boolean isIspre() {
		return ispre;
	}
	public void setIspre(boolean ispre) {
		this.ispre = ispre;
	}
	public String getPasswordcredit() {
		return passwordcredit;
	}
	public void setPasswordcredit(String passwordcredit) {
		this.passwordcredit = passwordcredit;
	}
	public ArrayList<Place> getMyPlaces() {
		return myPlaces;
	}
	public void setMyPlaces(ArrayList<Place> myPlaces) {
		this.myPlaces = myPlaces;
	}
	public ArrayList<User> getFriends() {
		return friends;
	}
	public void setFriends(ArrayList<User> friends) {
		this.friends = friends;
	}
	public ArrayList<String> getTastes() {
		return tastes;
	}
	public void setTastes(ArrayList<String> tastes) {
		this.tastes = tastes;
	}
	public ArrayList<CheckIn> getCheckIns() {
		return CheckIns;
	}
	public void setCheckIns(ArrayList<CheckIn> checkIns) {
		CheckIns = checkIns;
	}
	
	

}
