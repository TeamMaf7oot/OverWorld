package home;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import overworld.DBH;

public class NotificationController {
    ArrayList<Notification> notis=new ArrayList<>();
    public void markAsSeen(int index){
            notis.get(index).setSeen(true);
    }
    public void deleteNotification(int index){////////// delete from data base
            notis.remove(index);
    }
    public ArrayList<Notification> getNotificationfromDB(){
        ResultSet rs = DBH.excuteQuery("SELECT * FROM Notification where is_seen = 'false' and (kind = 'brand' or kind = 'check_in')");
        System.out.println("overworld.NotificationController.getNotificationfromDB()--------->");
        try {System.out.println("now ok");
            while(rs.next()){
                System.out.println(rs.getInt("noti_id"));
                notis.add(new Notification(rs.getString("kind"),rs.getString("date"), rs.getString("text")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(NotificationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return notis;
    }
    public void goToNotiSource(int index){
        notis.get(index).setSeen(true);
        //ResultSet
    }
}
