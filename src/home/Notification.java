package home;

import java.sql.Date;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import jdk.nashorn.internal.ir.RuntimeNode;
public class Notification {

    String kind;
    String date;
    boolean isSeen;
    String text;
    public Notification(String k){
        kind=k;
    }
    public Notification() {
        this.kind = null;
        this.date = null;
        this.text = null;
//        System.out.println(date);
    }
public Notification(String kind, String date, String text) {
        this.kind = kind;
        this.date = date;
        this.text = text;
//        System.out.println(date);
    }

    @Override
    public String toString() {
        return "kind=" + kind +  ", text=" + text + ", date=" + date+ '}';
    }
    public Notification(String kind, String text) {
        this.kind = kind;
        SimpleDateFormat d=new SimpleDateFormat();
        date=d.toString();
        this.text = text;
//        System.out.println(date);
    }
    public String getKind() {
            return kind;
    }
    public void setKind(String kind) {

            this.kind = kind;
    }
    public String getDate() {
            return date;
    }
    public void setDate(String date) {
            this.date = date;
    }
    public boolean isSeen() {
            return isSeen;
    }
    public void setSeen(boolean isSeen) {
            this.isSeen = isSeen;
    }
    public String getText() {
            return text;
    }
    public void setText(String text) {
            this.text = text;
    }
    public void insertInDB(){
        String query ="insert into Notification (kind ,date,text) values('"
                + kind+"',"
                + date+",'"
                + text+"'";
        Connection con=null;
        Statement st=null;
        ResultSet rs=null;
        try{
            Class.forName("org.sqlite.JDBC");
            con= DriverManager.getConnection("jdbc:sqlite:OverWorld.sqlite");
            st=con.createStatement();
            rs=st.executeQuery(query);
            System.out.println(rs);
        }catch(Exception ex){
            System.out.println(ex);
        }
    }
	
}
